import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import net.loogn.stardust.client.StardustClient;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.*;
import java.util.logging.ConsoleHandler;

/**
 * Created by Administrator on 2017/4/6.
 */

public class Program {


    public static void main(String[] args) throws Exception {
//        Method[] methods = Program.class.getDeclaredMethods();
//        Method method = methods[1];
//        Program program = Program.class.newInstance();
//
//
//        String json = "[{ \"id\":2 }]";
//        Class[] cls = method.getParameterTypes();
//        Type[] types = method.getGenericParameterTypes();
//
//        Object list = JSON.parseObject(json, types[0]);
//
//        Object obj = method.invoke(program, list);
//
//        System.out.println(obj);


//        public String F1(int id1, Integer id2, String name, Date date) {
//            return "F1:" + id1 + "," + id2 + "," + name;
//        }

        Package aPackage = Package.getPackage("");



        StardustClient.setConfigCenterUrl("http://localhost:85"); //运行时初始化一次
        try {
            StardustClient node1Client = new StardustClient("node2", "1.2");
            User user1 = new User();
            user1.setName("Update Name");
            user1.setAddTime(new Date());
            user1.setId(22);

            User user2 = new User();
            user2.setName("Update Name");
            user2.setAddTime(new Date());
            user2.setId(23);

            List<User> list = new ArrayList<>();
            list.add(user1);
            list.add(user2);

            Map map = new HashMap();

            map.put("x", 23.2d);
            map.put("y", 5.5d);


            User json = node1Client.invoke(User.class, "user", "updateuser", "王小明", 500000); //返回JSON字符串

            System.out.println(JSON.toJSONString(json));
        } catch (Exception exp) {
            System.out.println(exp.getMessage());
        }
    }
}
