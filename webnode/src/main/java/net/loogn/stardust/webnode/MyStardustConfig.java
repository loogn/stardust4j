package net.loogn.stardust.webnode;

import net.loogn.stardust.server.StardustConfig;
import net.loogn.stardust.webnode.services.CalcService;
import net.loogn.stardust.webnode.services.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/4/11.
 */
public class MyStardustConfig extends StardustConfig {

    public void setConfig(ConfigSetter setter) {
        setter.setVersion("1.2")
                .setServiceName("服务2")
                .setAddress("127.0.0.1:8086")
                .setConfigCenterUrl("http://localhost:85");
    }

    public void addService(ServiceAddor addor) {
        addor.add(UserService.class);
        addor.add(CalcService.class);
    }
}
