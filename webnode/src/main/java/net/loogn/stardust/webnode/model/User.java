package net.loogn.stardust.webnode.model;

import java.util.Date;

/**
 * Created by Administrator on 2017/4/26.
 */
public class User {
    private int id;
    private String name;
    private Date addTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
