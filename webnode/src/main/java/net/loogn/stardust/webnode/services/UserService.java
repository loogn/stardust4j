package net.loogn.stardust.webnode.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import net.loogn.stardust.server.StardustName;
import net.loogn.stardust.webnode.model.User;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/4/11.
 * 服务方法只能有0个参数或1个参数
 */
@StardustName(name = "user", description = "用户服务")
public class UserService {
    //可以没有参数
    @StardustName(name = "getStr")
    public String GetStr() {
        return "Hello UserService";
    }

    //可以有一个参数
    public List<User> F2(List<User> userList) {
        return userList;
    }

    public User F1(User user) {
        System.out.println(JSON.toJSONString(user));
        return user;
    }

    //可以有一个String参数接收JSON数据
    public User UpdateUser(String name) {
        User user = new User();
        user.setName(name);
        user.setAddTime(new Date());
        user.setId(name.length());
        return user;
    }
}

