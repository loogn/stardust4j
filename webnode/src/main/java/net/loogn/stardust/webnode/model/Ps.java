package net.loogn.stardust.webnode.model;

/**
 * Created by Administrator on 2017/4/27.
 */
public class Ps {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
