package net.loogn.stardust.webnode.services;

import net.loogn.stardust.server.StardustName;
import net.loogn.stardust.webnode.model.Ps;

/**
 * Created by Administrator on 2017/4/27.
 */

@StardustName(description = "计算器")
public class CalcService {


    @StardustName(description = "计算两个数相加")
    public double add(Ps ps) {
        return ps.getX() + ps.getY();
    }

    public double reduce(Ps ps) {
        return ps.getX() - ps.getY();
    }

    public double multiply(Ps ps) {
        return ps.getX() * ps.getY();
    }

    public double divide(Ps ps) {
        return ps.getX() / ps.getY();
    }
}

