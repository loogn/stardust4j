package net.loogn.stardust.common.enums;

/**
 * Created by Administrator on 2017/4/6.
 */
public enum ServerNodeStatus {
    Useless(0),
    Normal(1),
    Disconnect(2),
    Disabled(3);

    private int typeId;

    ServerNodeStatus(int i) {
        typeId = i;
    }
}
