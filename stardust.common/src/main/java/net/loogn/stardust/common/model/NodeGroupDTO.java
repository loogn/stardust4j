package net.loogn.stardust.common.model;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/4/6.
 */
public class NodeGroupDTO {
    private List<ServerNodeModel> nodes;
    private long maxEventId;
    private Date lastInvokeTime;

    public List<ServerNodeModel> getNodes() {
        return nodes;
    }

    public void setNodes(List<ServerNodeModel> nodes) {
        this.nodes = nodes;
    }

    public long getMaxEventId() {
        return maxEventId;
    }

    public void setMaxEventId(long maxEventId) {
        this.maxEventId = maxEventId;
    }

    public Date getLastInvokeTime() {
        return lastInvokeTime;
    }

    public void setLastInvokeTime(Date lastInvokeTime) {
        this.lastInvokeTime = lastInvokeTime;
    }
}
