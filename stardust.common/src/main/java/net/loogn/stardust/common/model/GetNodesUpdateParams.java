package net.loogn.stardust.common.model;

/**
 * Created by Administrator on 2017/4/6.
 */
public class GetNodesUpdateParams {
    private String serviceName;
    private long maxEventId;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public long getMaxEventId() {
        return maxEventId;
    }

    public void setMaxEventId(long maxEventId) {
        this.maxEventId = maxEventId;
    }
}
