package net.loogn.stardust.common.model;

import net.loogn.stardust.common.enums.ServerNodeStatus;

import java.util.Date;

/**
 * Created by Administrator on 2017/4/6.
 */
public class ServerNodeModel {
    private long id;
    private String serviceName;
    private String address;
    private String version;
    private ServerNodeStatus status;
    private double weight;
    private double dynamicWeight;
    private Date heartbeatTime;

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    private String platform;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public ServerNodeStatus getStatus() {
        return status;
    }

    public void setStatus(ServerNodeStatus status) {
        this.status = status;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getDynamicWeight() {
        return dynamicWeight;
    }

    public void setDynamicWeight(double dynamicWeight) {
        this.dynamicWeight = dynamicWeight;
    }

    public Date getHeartbeatTime() {
        return heartbeatTime;
    }

    public void setHeartbeatTime(Date heartbeatTime) {
        this.heartbeatTime = heartbeatTime;
    }

    public String getServiceUrl(String controllerName, String actionName) {
        return "http://" + address + "/stardust/" + controllerName + "/" + actionName;
    }
}
