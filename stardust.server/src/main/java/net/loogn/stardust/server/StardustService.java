package net.loogn.stardust.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import net.loogn.stardust.common.MessageResult;
import net.loogn.stardust.common.model.ServerNodeModel;
import net.loogn.stardust.common.model.ServiceContractModel;
import net.loogn.stardust.common.utils.HttpHelper;

import java.io.IOException;
import java.util.*;

/**
 * Created by Administrator on 2017/4/12.
 */
public class StardustService {
    private static String _configCenterUrl;
    private static ServerNodeModel _node;
    private static String _stardustRoot;
    private static Set<Class> _serviceClass;
    private static Timer taskTimer;

    static boolean register(StardustConfig config) {
        config.loadConfig();
        _node = new ServerNodeModel();
        _node.setVersion(config._version);
        _node.setServiceName(config._serviceName);
        _node.setAddress(config._address);
        _node.setPlatform("JAVA");
        _configCenterUrl = config._configCenterUrl;
        _stardustRoot = config._stardustRoot;
        _serviceClass = config._serviceSet;
        _serviceClass.add(__InternalService.class);


        try {
            String json = HttpHelper.postJsonToUrl(getRegisterUrl(), _node);
            MessageResult<ServerNodeModel> mr = JSON.parseObject(json, new TypeReference<MessageResult<ServerNodeModel>>() {
            });
            if (mr.isSuccess()) {
                _node = mr.getData();
                List<ServiceContractModel> contractList = ActionManager.loadActions(_node, _serviceClass);
                uploadContract(contractList);
                startTasks();
            }
            return mr.isSuccess();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        } catch (InstantiationException e) {
            e.printStackTrace();
            return false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    static void logout() {
        if (_node.getId() > 0) {
            String json = "{ \"nodeId\":" + _node.getId() + ",\"serviceName\":\"" + _node.getServiceName() + "\" }";
            try {
                HttpHelper.postJsonToUrl(getLogoutUrl(), json);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void uploadContract(List<ServiceContractModel> contractList) {
        try {
            HttpHelper.postJsonToUrl(getUploadContractUrl(), contractList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void startTasks() {
        taskTimer = new Timer();
        taskTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                String json = "{\"nodeId\":" + _node.getId() + "}";
                try {
                    HttpHelper.postJsonToUrl(getHeartbeatUrl(), json);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 5000);
    }


    public ServerNodeModel getNode() {
        return _node;
    }

    public static String getStardustRoot() {
        return _stardustRoot;
    }

    private static String getRegisterUrl() {
        return _configCenterUrl + "/StardustConfigCenter/Register";
    }

    private static String getHeartbeatUrl() {
        return _configCenterUrl + "/StardustConfigCenter/Heartbeat";
    }

    private static String getLogoutUrl() {
        return _configCenterUrl + "/StardustConfigCenter/Logout";
    }

    private static String getUploadContractUrl() {
        return _configCenterUrl + "/StardustConfigCenter/UploadContract";
    }

    public static String getUniqueNodeName() {
        if (_node == null) return "";
        return _node.getServiceName() + "," + _node.getVersion() + "," + _node.getAddress();
    }
}

