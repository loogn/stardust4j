package net.loogn.stardust.server;

import java.lang.annotation.*;

/**
 * Created by Administrator on 2017/4/12.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface StardustName {
    String name() default "";

    String description() default "";
}
