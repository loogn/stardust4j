package net.loogn.stardust.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import net.loogn.stardust.common.MessageResult;
import net.loogn.stardust.common.model.ServerNodeModel;
import net.loogn.stardust.common.utils.HttpHelper;

import java.io.IOException;
import java.util.*;

/**
 * Created by Administrator on 2017/4/7.
 */
public abstract class StardustConfig {

    String _configCenterUrl;
    String _serviceName;
    String _stardustRoot;
    String _address;
    String _version;
    Set<Class> _serviceSet;

    void loadConfig() {

        StardustConfig.ConfigSetter configSetter = new ConfigSetter();
        setConfig(configSetter);
        StardustConfig.ServiceAddor serviceAddor = new ServiceAddor();
        addService(serviceAddor);
    }

    public StardustConfig() {
        _serviceSet = new HashSet<Class>();
    }

    public abstract void setConfig(ConfigSetter setter);

    public abstract void addService(ServiceAddor addor);

    public final class ConfigSetter {
        public ConfigSetter setConfigCenterUrl(String configCenterUrl) {
            _configCenterUrl = configCenterUrl;
            return this;
        }

        public ConfigSetter setServiceName(String serviceName) {
            _serviceName = serviceName;
            return this;
        }

        public ConfigSetter setAddress(String address, String serviceRoot) {
            if (serviceRoot == null || serviceRoot.length() == 0) {
                _stardustRoot = "/stardust";
                _address = address;
            } else {
                _stardustRoot = "/" + serviceRoot.toLowerCase() + "/stardust";
                _address = address + "/" + serviceRoot;
            }
            return this;
        }

        public ConfigSetter setAddress(String address) {
            return setAddress(address, "");
        }

        public ConfigSetter setVersion(String version) {
            _version = version;
            return this;
        }
    }

    public final class ServiceAddor {
        public void add(Class serviceClass) {
            _serviceSet.add(serviceClass);
        }
    }

}
